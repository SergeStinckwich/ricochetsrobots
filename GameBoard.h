/*The MIT License (MIT)

Copyright (c) <2013> <Serge Stinckwich>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#ifndef _GAMEBOARD_
#define _GAMEBOARD_

#define WALL '+'
#define ROBOT 'R'

typedef struct GameBoard {
  int x_size;
  int y_size;
  char** cells;
} GameBoard;

GameBoard* GameBoard_new();
int GameBoard_get_x_size(GameBoard*);
int GameBoard_get_y_size(GameBoard*);
int GameBoard_HasAWallAtPosition(GameBoard*, int, int);
void GameBoard_addExternalWalls(GameBoard*);
void GameBoard_initializeWithRandomObstacles(GameBoard*);
void GameBoard_printOnScreen(GameBoard*);
void GameBoard_initializeRobotAtRandomPosition(GameBoard*);

#endif
