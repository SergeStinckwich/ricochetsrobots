/*The MIT License (MIT)

Copyright (c) <2013> <Serge Stinckwich>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#include <stdlib.h>
#include <stdio.h>
#include "GameBoard.h"

GameBoard* GameBoard_new() {
  GameBoard* b = (GameBoard*) malloc(sizeof(GameBoard));
  b->x_size = 33;
  b->y_size = 33;
  b->cells = (char**) malloc(sizeof(char)*b->x_size*b->y_size);
  return b;
}

int GameBoard_get_x_size(GameBoard *b) {
  return b->x_size;
}

int GameBoard_get_y_size(GameBoard *b) {
  return b->y_size;
}

int GameBoard_HasAWallAtPosition(GameBoard *b, int x, int y) {
  return (b->cells[x][y]==WALL);
}

void GameBoard_addWallAtPosition(GameBoard *b, int x, int y) {
  b->cells[x][y] = WALL;
}

void GameBoard_addRobotAtPosition(GameBoard *b, int x, int y) {
  b->cells[x][y] = ROBOT;
}

void GameBoard_addExternalWalls(GameBoard *b) {
  for (int i = 0; i < GameBoard_get_x_size(b); i++) {
    GameBoard_addWallAtPosition(b, 0, i);
    GameBoard_addWallAtPosition(b, GameBoard_get_y_size(b)-1, i);
  }
  for (int j = 0; j < GameBoard_get_y_size(b); j++) {
    GameBoard_addWallAtPosition(b, j, 0);
    GameBoard_addWallAtPosition(b, j, GameBoard_get_x_size(b)-1);
  }
}

void GameBoard_initializeWithRandomObstacles(GameBoard *b) {
  GameBoard_addExternalWalls(b);
  GameBoard_initializeRobotAtRandomPosition(b);
}

void GameBoard_initializeRobotAtRandomPosition(GameBoard *b) {
  int x = rand()%GameBoard_get_x_size(b);
  int y = rand()%GameBoard_get_y_size(b);
  GameBoard_addRobotAtPosition(b, x, y);
}

void GameBoard_printOnScreen(GameBoard *b) {
  for (int i=0; i<GameBoard_get_x_size(b); i++) {
    for (int j=0; j<GameBoard_get_y_size(b); j++)
      if (b->cells[i][j] == WALL) printf ("%s", "+ ");
      else 
	if (b->cells[i][j] == ROBOT) printf("%s", "R ");
	else printf("%s", "  ");
    printf("\n");
  }
}
