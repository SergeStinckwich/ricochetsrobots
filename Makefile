all: main test

clean:
	rm *.o main TestGameManager TestGameBoard

test: TestGameManager TestGameBoard

TestGameManager: TestGameManager.o GameManager.o Player.o
	gcc -std=c99 TestGameManager.o Player.o GameManager.o -o TestGameManager -lcunit

TestGameManager.o: TestGameManager.c GameManager.h
	gcc -std=c99 -c TestGameManager.c

TestGameBoard: TestGameBoard.o GameBoard.o
	gcc -std=c99 TestGameBoard.o GameBoard.o -o TestGameBoard -lcunit

TestGameBoard.o: TestGameBoard.c GameBoard.h
	gcc -std=c99 -c TestGameBoard.c

GameManager.o:	GameManager.c
	gcc -std=c99 -c GameManager.c

GameBoard.o: GameBoard.c GameBoard.h
	gcc -std=c99 -c GameBoard.c

Player.o: Player.c
	gcc -std=c99 -c Player.c

main: main.o Player.o GameManager.o GameBoard.o
	gcc -std=c99 main.o -o main GameManager.o GameBoard.o

main.o: main.c Player.h GameManager.h GameBoard.h
	gcc -std=c99 -c main.c