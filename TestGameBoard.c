/*The MIT License (MIT)

Copyright (c) <2013> <Serge Stinckwich>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#include <stdio.h>
#include "CUnit/Basic.h" 

#include "GameBoard.h"

void testSizeOfANewBoardIs33(void) {
  GameBoard* g = GameBoard_new();
  
  CU_ASSERT((GameBoard_get_x_size(g) == 33) &&
	    (GameBoard_get_y_size(g) == 33)
	    );
}

void testAfterAddingExternalWallThereIsAWallOnEachCorners(void) {
  GameBoard* g = GameBoard_new();
  GameBoard_addExternalWalls(g);

  CU_ASSERT(GameBoard_HasAWallAtPosition(g, 0, 0) &&
	    GameBoard_HasAWallAtPosition(g, GameBoard_get_x_size(g)-1, 0) &&
	    GameBoard_HasAWallAtPosition(g, 0, GameBoard_get_y_size(g)) &&
	    GameBoard_HasAWallAtPosition(g, GameBoard_get_x_size(g)-1, GameBoard_get_y_size(g)-1)
	    )
}

int main (int argc, char** argv) {

	CU_pSuite pSuite = NULL;
 
    /* initialize the CUnit test registry */ 
    if (CUE_SUCCESS != CU_initialize_registry())
    	return CU_get_error();
 
   /* add a suite to the registry */ 
   pSuite = CU_add_suite("Game Board", NULL, NULL);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */ 
   if (NULL == CU_add_test(pSuite, "A new board has a size of 33", testSizeOfANewBoardIs33)) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if (NULL == CU_add_test(pSuite, "After adding external, corners are walls", testAfterAddingExternalWallThereIsAWallOnEachCorners)) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */ 
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
